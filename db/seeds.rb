
Channel.insert_all([{ name: 'general' }, { name: 'gaming' }])

id_1, id_2 = Channel.all.pluck(:id)
Message.insert_all([{ content: 'Aright', author: 'Dave', channel_id: id_1 },
                    { content: 'yup', author: 'Rick', channel_id: id_1 },
                    { content: 'why hello daring', author: 'Amy', channel_id: id_2 },
                    { content: 'good day fine fellow lady', author: 'Zeo', channel_id: id_2 }])
