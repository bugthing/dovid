class MessagesController < ApplicationController
  before_action :set_channel, only: [:create]
  attr_reader :channel

  def create
    Message.create(message_params.merge(author: 'dunno', channel: channel))

    render partial: 'messages/form'
  end

  private

  def set_channel
    @channel = Channel.find(params[:channel_id])
  end

  def message_params
    params.require(:message).permit(:content)
  end

end
