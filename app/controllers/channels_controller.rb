class ChannelsController < ApplicationController
  before_action :set_channels
  before_action :set_channel, only: %i[ show destroy ]

  # GET /channels
  def index
  end

  # GET /channels/1
  def show
    render action: :index
  end

  # POST /channels
  def create
    @channel = Channel.create(channel_params)

    render partial: 'channels/form'
  end

  # DELETE /channels/:id
  def destroy
    @channel.destroy
    respond_to do |format|
      format.turbo_stream { render turbo_stream: turbo_stream.remove(@channel) }
      format.html         { redirect_to channels_url }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_channel
      @channel = Channel.find(params[:id])
    end

    def set_channels
      @channels = Channel.all.order(name: :asc)
    end

    # Only allow a list of trusted parameters through.
    def channel_params
      params.require(:channel).permit(:name)
    end
end
