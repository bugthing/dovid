Rails.application.routes.draw do
  resources :channels do
    resources :messages, only: [:create]
    resource :tooltip, only: [:show]
  end

  root 'channels#index'
end
