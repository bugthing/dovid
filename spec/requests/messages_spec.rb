require 'rails_helper'

RSpec.describe "Channel Messages", type: :request do
  describe "POST /channels/:channel_id/messages" do
    it "returns http success" do
      channel = Channel.create name: "test"
      post channel_messages_path(channel), params: { message: { content:"Hello World" } }
      expect(response).to have_http_status(:success)
    end
  end

end
