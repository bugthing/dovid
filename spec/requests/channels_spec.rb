require 'rails_helper'

RSpec.describe "/channels", type: :request do
  
  let(:valid_attributes) do
    {
      name: "Test Channel",
    }
  end

  describe "GET /index" do
    it "renders a successful response" do
      Channel.create! valid_attributes
      get channels_url
      expect(response).to be_successful
    end
  end

  describe "GET /show" do
    it "renders a successful response" do
      channel = Channel.create! valid_attributes
      get channel_url(channel)
      expect(response).to be_successful
    end
  end

  describe "POST /create" do
    context "with valid parameters" do
      it "creates a new Channel" do
        expect {
          post channels_url, params: { channel: valid_attributes }
        }.to change(Channel, :count).by(1)
      end

      it "renders the channel form again" do
        post channels_url, params: { channel: valid_attributes }
        expect(response).to be_successful
        expect(response.body).to include('<form')
      end
    end
  end

end
