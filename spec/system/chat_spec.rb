require "rails_helper"

RSpec.describe "Chat" do
  it "shows me the channels and when I click the messages are displayed" do
    message = Message.create(content: "Hello", channel: Channel.create(name: "general"), author: "test")

    visit "/"

    expect(page).to have_text("Channels").and(have_text("No channel selected"))

    click_on "general"

    expect(page).to have_text("Channels").and(have_text("Hello"))
  end
end
